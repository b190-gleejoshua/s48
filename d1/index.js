let posts = [];
let count = 1;

// adding posts

document.querySelector("#form-add-post").addEventListener("submit",(e)=>{
	// prevents the page from reloading after a button has been clicked/triggered
	e.preventDefault();
	// adds the informatio from the "frontend" to our mock database
	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});


	count++;


	showPosts(posts);
	alert("Successfully Added");
});


// showPosts()
const showPosts = (posts) => {
	let postEntries = '';

	// we use forEach method so that each of the JSON inside our mock databse will be rendered insdide the #div-post-entries
	posts.forEach((post)=>{
		// in javscript it is also possible to render html elements with the use of function methods plus appropoeaite syntax such as template literals(backticks plus ${})
		postEntries += `
			<div id = "post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost(${post.id})">Edit</button>
				<button onclick="deletePost(${post.id})">Delete</button>
			</div>
		`
	})

	// the div duplicates the objects inside the postEntries variable
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

const editPost = (id) =>{
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}
// update post

document.querySelector("#form-edit-post").addEventListener("submit", (e)=>{
	e.preventDefault();

	for(let i=0; i<posts.length; i++){
		// the value of posts[i].id is a Number but the value for document.querySelector("#txt-edit-id").value is a string
		if (posts[i].id.toString()=== document.querySelector("#txt-edit-id").value){
			posts[i].title = document.querySelector("#txt-edit-title").value
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPosts(posts);
			alert("Successfully updated");


			break
		}
	}
})

const deletePost = (id) =>{
	const element = document.querySelector("#div-post-entries")

	element.remove()
	alert("Deleting Successful")
}